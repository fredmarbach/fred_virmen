
function vr = towersII_trajectoryPlot(vr,action) 

switch action
    
    % -------------------------------------------------    
    case 'init'
    % -------------------------------------------------    
    
        % --- initialise axes ---
        vr.myplots.traj_ax = axes(vr.myplots.figureHandle,'Position',[0.1 0.15 0.3 0.35]); % [x y w h]
        
        axes(vr.myplots.traj_ax);
        
        % --- plot line indicating 500mm/s speed ---
        standardSpeed = 500;
        thisPos = [0 vr.S.GUI.yThreshold];
        thisTime = [0 vr.S.GUI.yThreshold/standardSpeed];
        plot(thisTime,thisPos,'k--');
        
        hold on;
        
        % --- get nTrajVisible colors ready ---
        vr.myplots.traj_colors = brewermap(vr.S.GUI.nTrajVisible,'*RdPu');
        
        
    % -------------------------------------------------    
    case 'updateTrajectory'
    % -------------------------------------------------    
        
        tmp_ax = gca;
        axes(vr.myplots.traj_ax);
        
        % --- plot new trajectory ---
        tmp_ind = vr.data.trialNumber == vr.tmp.currentTrial;
        tmp_pos = vr.data.pos(tmp_ind,:);
        tmp_time = vr.data.time(tmp_ind);
        thisTime = tmp_time - tmp_time(1);
        plot(thisTime,tmp_pos(:,2),'k-','Parent',vr.myplots.traj_ax);
        
        % --- delete old trajectory and recolor others ---
        for ii = 1:min(vr.S.GUI.nTrajVisible,vr.tmp.currentTrial)
            if ii == vr.S.GUI.nTrajVisible
                delete(vr.myplots.traj_ax.Children(ii));
            else
                vr.myplots.traj_ax.Children(ii).Color = vr.myplots.traj_colors(ii,:);
            end
        end
        
        axes(tmp_ax);
        
        
end

















