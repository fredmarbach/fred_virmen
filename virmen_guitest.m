% starts a virmen session

taskName = 'towers';
mouse = 'test';

virmenDataRoot = 'D:\virmenData';
settingsPath = fullfile(virmenDataRoot,'settings');
dataPath = fullfile(virmenDataRoot,'data');

S = virmen_taskParams(taskName,mouse,settingsPath);

% Initialize parameter GUI plugin and Pause
[virmenGUI,S] = virmen_paramGUI(S,'init');



[virmenGUI,S] = virmen_paramGUI(S,'sync',virmenGUI);