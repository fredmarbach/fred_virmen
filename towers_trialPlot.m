% deals with the trialPlot
% plots trial history with colors denoting performance

function vr = towers_trialPlot(vr,action)

switch action
    
    % -------------------------------------------------
    case 'init'
    % -------------------------------------------------
        % --- initialise axes ---
        tmp_H = 0.2;
        vr.myplots.trials_ax = axes(vr.myplots.figureHandle,...
                                    'Position',[0.1 1-tmp_H-0.05 0.6 tmp_H]); % [x y w h]
        
        tmp = brewermap(9,'Set1');
        c_hit = tmp(3,:);
        c_miss = tmp(1,:);
        c_timeout = tmp(5,:);
        c_neutral = tmp(9,:);
        vr.myplots.trialsColors = cat(1,c_neutral,c_hit,c_miss,c_timeout);
        vr.myplots.trialsSymbols = 'oooo';
        vr.myplots.trialsSizes = 6*[1 1 1 1];
  
        
    % -------------------------------------------------    
    case 'update'
    % -------------------------------------------------
            
        tmp_ax = gca;
        axes(vr.myplots.trials_ax);

        % vr.data.trials.correct is all nans from start
        % 1 = correct, 0 = error, -1 = timeout, nan = future
        % color order is neutral-hit-miss-timeout
        xdata = 1:vr.S.GUI.maxTrials;
        ydata = vr.params.actualEvidence;
        
        types = [nan 1 0 -1];
        for ii = 1:length(types)
            if isnan(types(ii))
                tmp_trials = isnan(vr.data.trial.correct);
            else
                tmp_trials = vr.data.trial.correct == types(ii);
            end
            plot(xdata(tmp_trials),ydata(tmp_trials),vr.myplots.trialsSymbols(ii),...
                       'MarkerSize',vr.myplots.trialsSizes(ii),...
                       'MarkerFaceColor',vr.myplots.trialsColors(ii,:),...
                       'MarkerEdgeColor','none');
            hold on;
        end
        
        xlimits = cellfun(@str2double,strsplit(vr.S.GUI.nTrialsVisible,' '));
        xlim(xlimits+vr.tmp.currentTrial);
        ylim([-1.15 1.15]);
        xlabel('Trials');
        ylabel('Left tower rate');
        vr.myplots.trials_ax.TickDir = 'out';
        vr.myplots.trials_ax.Box = 'off';
        
        % display leftProb as text
        text(vr.tmp.currentTrial+xlimits(1)+3,0,...
                        ['leftProb = ' num2str(vr.S.GUI.leftProb)],...
                        'FontSize',10);
        
        hold off;
        % switch back to previous axis 
        axes(tmp_ax);
                
end















