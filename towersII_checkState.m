
function vr = towersII_checkState(vr,action)


% this is executed every iteration - no heavy stuff!
switch action
    % -------------------------------------------------
    case 'midTrial'
    % -------------------------------------------------
        if vr.position(2) > vr.S.GUI.yThreshold % mouse reached end of tunnel
            % send TTL to bpod to make it react to licks
            vr = towersII_DAQ(vr,'toBpod_end');
            % make world black, changes vr.tmp.black == true
            vr = virmen_black(vr,'black');
            vr.tmp.currentState = 'waitForBpod';
        end

        % --- check if total time is up ---
        vr = checkTimeOut(vr,'a');
        

    % ------------------------------------------------- 
    case 'waitForBpod'
    % -------------------------------------------------   
        
        % read TTL from bpod (which spout was licked)
        vr = towersII_DAQ(vr,'fromBpod');

        % mouse licked left
        if vr.daq.fromBpod_session(1) == 1 && vr.daq.fromBpod_session(2) == 0
            vr.data.trial.outcome(vr.tmp.currentTrial) = 1;
            if vr.params.actualEvidence(vr.tmp.currentTrial) <= 0
                vr.data.trial.correct(vr.tmp.currentTrial) = 0;
            else
                vr.data.trial.correct(vr.tmp.currentTrial) = 1;
            end
            vr.tmp.currentState = 'ITI';
        % mouse licked right
        elseif vr.daq.fromBpod_session(1) == 0 && vr.daq.fromBpod_session(2) == 1
            vr.data.trial.outcome(vr.tmp.currentTrial) = 2;
            if vr.params.actualEvidence(vr.tmp.currentTrial) <= 0
                vr.data.trial.correct(vr.tmp.currentTrial) = 1;
            else
                vr.data.trial.correct(vr.tmp.currentTrial) = 0;
            end
            vr.tmp.currentState = 'ITI';
        else
            % --- check if total time is up ---
            vr = checkTimeOut(vr,'b');
        end
        
        % --- if we just switched to ITI state execute this once ---
        if strcmp(vr.tmp.currentState,'ITI')
            % turn normal world back on 
            vr = virmen_black(vr,'visible');
            % switch to ITI world
            vr.currentWorld = 2; 
            vr.position([1 4]) = 0; % put mouse at starting position
            vr.dp(:) = 0; 
            if vr.data.trial.correct(vr.tmp.currentTrial) == 0 % error
                % put mouse at y position -vr.S.GUI.punish
                vr.position(2) = -vr.S.GUI.punishLength;
            else % put mouse at +30 (to avoid seeing the punishTunnel)
                vr.position(2) = 30;
            end  
        end

        
    % -------------------------------------------------
    case 'ITI'
    % -------------------------------------------------

        % --- check if ITI is over ---
        % if so switch back to normal world
        if vr.position(2) > vr.S.GUI.ITILength
            
            % --- switch to last state ---
            % this is used in main loop for trial updating
            vr.tmp.currentState = 'endOfTrial';
        else
            % --- check if total time is up ---
            vr = checkTimeOut(vr,'b');
        end
end
end


% ---------------------------------------------
% --- local function to declutter the above ---
% ---------------------------------------------
function vr = checkTimeOut(vr,action)

switch action
    case 'a'
        % check if max trial time is up
        if vr.timeElapsed - vr.tmp.trialStartTime > vr.S.GUI.maxTrialTime % mouse timed out
            vr.tmp.currentState = 'endOfTrial';
            vr = towersII_DAQ(vr,'toBpod_end'); % Bpod needs 2 of these triggers to go into 'timeout' state and restart statematrix
            vr = towersII_DAQ(vr,'toBpod_end');
            vr.data.trial.outcome(vr.tmp.currentTrial) = -1;
            vr.data.trial.correct(vr.tmp.currentTrial) = -1;
        end
        
    case 'b'
        % check if max trial time is up
        if vr.timeElapsed - vr.tmp.trialStartTime > vr.S.GUI.maxTrialTime % mouse timed out
            vr.tmp.currentState = 'endOfTrial';
            vr = towersII_DAQ(vr,'toBpod_end'); % put bpod into 'timeout' state to restart the statematrix
            vr.data.trial.outcome(vr.tmp.currentTrial) = -1;
            vr.data.trial.correct(vr.tmp.currentTrial) = -1;
        end
end
end













