
function [displacement,movementType] = mvt_function_rotary(vr)

movementType = 'displacement';

% convert to virmen space units 
diffCount = vr.S.GUI.direction * (vr.mvt.currentCount - vr.mvt.previousCount);
displacement(2) = vr.mvt.distancePerTic * diffCount * vr.S.GUI.scalingY;
displacement(1) = 0;
displacement(3:4) = 0;
