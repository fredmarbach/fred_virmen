
function vr = towers_trajectoryPlot(vr,action) 

switch action
    
    % -------------------------------------------------    
    case 'init'
    % -------------------------------------------------    
    
        % --- initialise axes ---
        vr.myplots.traj_ax = axes(vr.myplots.figureHandle,'Position',[0 0.1 0.5 0.5]); % [x y w h]
        
        %[ho,he,ha] = draw2D(vr.exper.worlds{vr.currentWorld}); 

        % --- draw the world in 2D ---
        axes(vr.myplots.traj_ax);
        hold on; axis equal;
        allObjects = vr.exper.worlds{vr.currentWorld}.objects;
        % skip tower objects
        for ii = 1:length(allObjects)
            if sum(vr.towers.objectIndex == ii) == 0
                [~,~] = draw2D(vr.exper.worlds{1}.objects{ii});
            end
        end
        view(2) % view in 2D
        tmp_limits = getDataLimits(vr.myplots.traj_ax);
        ylim([vr.towers.mouseInitialY tmp_limits.y(2)]);

        %     ax_plot = axes('Position',plot_xywh,...
        %                    'xcolor',axisColor,'ycolor',axisColor,...
        %                    'Color',[0 0 0],'TickDir','out','Box','off',...
        %                    'FontSize',textSize);

        
        % get traj_nVisible colors ready
        vr.myplots.traj_colors = brewermap(vr.S.GUI.nTrajVisible,'*RdPu');
        
        
    % -------------------------------------------------    
    case 'updateTrajectory'
    % -------------------------------------------------    
        
        tmp_ax = gca;
        axes(vr.myplots.traj_ax);
        
        % --- plot new trajectory ---
        tmp_ind = vr.data.trialNumber == vr.tmp.currentTrial;
        tmp_pos = vr.data.pos(tmp_ind,:);
        plot(tmp_pos(:,1),tmp_pos(:,2),'k-','Parent',vr.myplots.traj_ax);
        
        % --- delete old trajectory and recolor others ---
        for ii = 1:min(vr.S.GUI.nTrajVisible,vr.tmp.currentTrial)
            if ii == vr.S.GUI.nTrajVisible
                delete(vr.myplots.traj_ax.Children(ii));
            else
                vr.myplots.traj_ax.Children(ii).Color = vr.myplots.traj_colors(ii,:);
            end
        end
        
        axes(tmp_ax);
        
        
    % -------------------------------------------------      
    case 'updateTowers'
    % -------------------------------------------------    
    
        tmp_ax = gca;
        axes(vr.myplots.traj_ax);
        
        % delete tower circles from previous trial
        if isfield(vr.myplots,'traj_towerHandles')
            delete(vr.myplots.traj_towerHandles);
        end
        
        thisColor = [0.2 0.7 0.2];
        circle_r = eval(vr.exper.variables.edgeRadius);
        for tt = 1:vr.towers.nActive
            % plot circle where tower is
            circle_x = vr.towers.x(tt);
            circle_y = vr.towers.y(tt);
            vr.myplots.traj_towerHandles(tt) = drawCircle(circle_x,circle_y,circle_r,thisColor);
        end
        
        axes(tmp_ax);
end

















