% check if all the params S.GUI were also added 
% to S.GUIPanels - if they don't match, throw error 

function [] = virmen_checkGUIPanels(S)

errorFlag = false;
thisFieldNames = fieldnames(S.GUIPanels);
nFields = length(thisFieldNames);
allPanelParams = [];
% make a cell array with all parameter names put in the GUI
for ii = 1:nFields
    thisPanelName = thisFieldNames{ii};
    allPanelParams = [allPanelParams S.GUIPanels.(thisPanelName)];
end
allSGUIParams = fieldnames(S.GUI);

% check if the GUI panels have all parameters from S.GUI

% these params are missing in GUI panels
% remove them from S.GUI
conflictingParams = setdiff(allSGUIParams,allPanelParams);
if ~isempty(conflictingParams)
    for cc = 1:length(conflictingParams)
        display(['---missing in S.GUI: ' conflictingParams{cc}])
    end
    errorFlag = true;
end

% these params were loaded but are missing in S.GUI / TrialSettings
% we will add the field to S.GUI
conflictingParams = setdiff(allPanelParams,allSGUIParams);
if ~isempty(conflictingParams)
    for cc = 1:length(conflictingParams)            
        display(['---missing in S.GUIPanels: ' conflictingParams{cc}])
    end
    errorFlag = true;
end

if errorFlag == true
    error('---please edit/complete virmen_initGUIParams.m---')
end