% initialise DAQ

function vr = towers_DAQ(vr,action)

switch action
    
    case 'init'
           
        % --- start session with analog input (photoDiode) ---
        vr.daq.ai_session = daq.createSession('ni');
        % photoDiode analog input
        addAnalogInputChannel(vr.daq.ai_session,vr.S.GUI.deviceName,0:2,'Voltage');
        
        % tmp log file ID
        vr.daq.logFile = 'D:\temp_towersLog.bin';
        delete(vr.daq.logFile); % try to delete file
        if exist(vr.daq.logFile,'file') == 2
            fclose all;
            delete(vr.daq.logFile);
            disp('---closed all files to delete logFile---')
        end
        vr.daq.logFileID = fopen(vr.daq.logFile,'a'); % 'a' for append
        
        % add listener to save stuff in background
        vr.daq.ai_listener = addlistener(vr.daq.ai_session,'DataAvailable',...
                             @(src,event) fwrite(vr.daq.logFileID,...
                             [event.TimeStamps, event.Data]','double'));
              
        vr.daq.ai_session.IsContinuous = true;
        vr.daq.ai_session.Rate = 200;
        vr.daq.ai_session.NotifyWhenDataAvailableExceeds = 200;
        vr.daq.ai_session.startBackground();

        
        % --- start session for counter input channels ---
        % 2 counter channels for rotary encoders
        vr.daq.ctr_session = daq.createSession('ni');
        vr.daq.ctr0_channel = addCounterInputChannel(vr.daq.ctr_session,vr.S.GUI.deviceName,...
                                                'ctr0','Position');
        vr.daq.ctr0_channel.InitialCount = vr.mvt.initialCount;
        %vr.daq.rotaryY = addCounterInputChannel(vr.daq.session,vr.S.GUI.deviceName,'ctr1','Position');
        
        
        % digital out to scanimage at trial start
        vr.daq.out_session = daq.createSession('ni');
        warning('off','daq:Session:onDemandOnlyChannelsAdded');
        addDigitalChannel(vr.daq.out_session,vr.S.GUI.deviceName,...
                                                 'Port1/Line1:2','OutputOnly');

       
    
    case 'endOfTrial'
        
        % --- write to scanimage ---
        outputSingleScan(vr.daq.out_session,[1 1])
        pause(0.001) % pause for 1ms
        outputSingleScan(vr.daq.out_session,[0 0]) 
        
        % reset counters to initialcount 1000 to avoid too high values (which would probably never happen?)
        resetCounters(vr.daq.ctr_session);
        vr.mvt.currentCount = double(vr.daq.ctr0_channel.InitialCount);
        
        
    case 'cleanUp'
        
        % --- close sessions etc. ---
        vr.daq.ai_session.stop();
        delete(vr.daq.ai_listener);
        release(vr.daq.ai_session);
        fclose(vr.daq.logFileID);
        disp('---closed files and daq session---')
        
end














        