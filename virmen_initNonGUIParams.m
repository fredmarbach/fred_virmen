% this is to de-clutter the 'initializationCodeFun'
% initialise parameters not saved and loaded
% these are not in the GUI for that reason
% generally also not task/subtask specific

function vr = virmen_initNonGUIParams(vr)


% ----------------------------------------
% --- get user to select settings file ---
% ----------------------------------------
inputData = inputdlg({'Mouse','Task'},'Choose',[1 35],{'mar100','towers'});
vr.params.mouse = inputData{1};
vr.params.task = inputData{2};


% --- main data/settings folder ---
vr.params.dataPath = 'D:\virmenData\data';
vr.params.savingPath = fullfile(vr.params.dataPath,vr.params.mouse);
vr.params.figureScreenshotPath = fullfile(vr.params.savingPath,'sessionPNG');
if exist(vr.params.figureScreenshotPath,'dir') == 0
    mkdir(vr.params.figureScreenshotPath);
end
vr.params.settingsPath = 'D:\virmenData\settings';
if exist(vr.params.settingsPath,'dir') == 0
    mkdir(vr.params.settingsPath);
end


% -------------------------------------
% --- initialise nonGUI parameters ----
% -------------------------------------
     
% --- init movement function variables ---
vr.mvt.axleRadius = 75; % in mm
vr.mvt.ticsPerRevolution = 1024; % rotary encoder specs
vr.mvt.distancePerTic = pi*2*vr.mvt.axleRadius / vr.mvt.ticsPerRevolution;
vr.mvt.initialCount = 100000; % to allow for enough backward movement
vr.mvt.previousCount = vr.mvt.initialCount;
vr.mvt.currentCount = vr.mvt.initialCount;

% --- init some other variables ---
vr.tmp.executionTime = 0;
vr.tmp.executionInd = 0;
vr.tmp.currentTrial = 1;
vr.tmp.endOfTrial = 0;
vr.tmp.trialStartTime = 0; % used to abort long trials
vr.tmp.black = false; % indicates if world is visible or black
vr.tmp.currentState = 'midTrial'; % indicates which state the trial is in

% --- init white square for photodiode ---
% Create a bunch of white lines top right corner
vr.myplots.photoDiode.size = 1; % Symbol size as fraction of the screen (only the actual part virmen uses if not in full screen mode)
tmp_nlines = 100;
tmp_x = -1:2/tmp_nlines:1;
tmp_y = -1 * ones(1,length(tmp_x));
tmp_y(2:2:end) = 1;
vr.plot(1).x = tmp_x * vr.myplots.photoDiode.size + 1 - vr.myplots.photoDiode.size;
vr.plot(1).y = tmp_y * vr.myplots.photoDiode.size + 1 - vr.myplots.photoDiode.size;
vr.plot(1).color = [1 1 1];

% --- init progress plots ---
vr.myplots.figureHandle = figure('Units','normalized',...
                                 'Position',[0 0.05 0.5 0.5],'Color',[1 1 1]);
movegui(vr.myplots.figureHandle,'southeast');
% trial history plot
% trajectory plot
% performance plot and psych curve
