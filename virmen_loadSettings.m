
function [S,failedToLoad] = virmen_loadSettings(vr)

% --- example file name: mar100_towers.mat ---
fileName = [vr.params.mouse '_' vr.params.task '.mat'];
mouseSettingsFile = fullfile(vr.params.settingsPath,fileName);

if exist(mouseSettingsFile,'file')==2
    tmp = load(mouseSettingsFile);
    disp('---loaded mouse settings---')
    S = tmp.S;
    failedToLoad = false;
else
    tmp = dir([vr.params.settingsPath filesep '*.mat']);
    allFiles = {tmp.name};
    [ind,ok] = listdlg('PromptString','no settings found--select or cancel','ListString',allFiles);
    if ok
        disp('---adopting settings from other mouse---')
        tmp = load(fullfile(vr.params.settingsPath,allFiles{ind}));
        S = tmp.S;
        failedToLoad = false;
    else
        disp('---no settings loaded---')
        S = struct; 
        failedToLoad = true;
    end
end

