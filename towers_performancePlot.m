% deals with the trialPlot
% plots trial history with colors denoting performance

function vr = towers_performancePlot(vr,action)

switch action
    
    % -------------------------------------------------
    case 'init'
    % -------------------------------------------------
        % --- initialise axes ---
        tmp_H = 0.35;
        vr.myplots.perform_ax = axes(vr.myplots.figureHandle,...
                                    'Position',[0.5 0.1 tmp_H tmp_H]); % [x y w h]

                                
    % -------------------------------------------------    
    case 'update'
    % -------------------------------------------------
            
        tmp_ax = gca;
        axes(vr.myplots.perform_ax);
        
        trials = 1:vr.tmp.currentTrial;
        
        bins = -1:vr.S.GUI.psychBin:1;
        bins(end) = 1; % make sure it spans -1 to 1
        
        xvec = -1+0.5*vr.S.GUI.psychBin:vr.S.GUI.psychBin:1-0.5*vr.S.GUI.psychBin;
        
        % 1 = correct, 0 = error, -1 = timeout, nan = future
        leftChoice = vr.data.trial.outcome(trials) == 2;
         
        % plot left/right percent correct at -1 and 1
        leftTrials = vr.params.actualEvidence(trials) < 0;
        leftPerf = sum(leftChoice(leftTrials))/sum(leftTrials);
        rightTrials = vr.params.actualEvidence(trials) > 0;
        rightPerf = sum(leftChoice(rightTrials))/sum(rightTrials);

        plot([-1 1],[leftPerf rightPerf],'ko','MarkerFaceColor',[0.65 0.45 1],...
                                         'Parent',vr.myplots.perform_ax);
        hold on;
        
        % n = counts per bin; ind = bin index for each trial
        [n,~,ind] = histcounts(vr.params.actualEvidence(trials),bins);
        
        % plot performance in bins
        binPerf = nan(1,length(n));
        for bb = 1:length(n) % loop over bins
            if n(bb) > 0
                binPerf(bb) = sum(leftChoice(ind==bb))/n(bb);
            end
        end
        plot(xvec,binPerf,'ko','MarkerFaceColor','k',...
                          'Parent',vr.myplots.perform_ax);
               
        % plot line at 50%
        line([0 0],[0 1],'Color',0.7*[1 1 1]);
        line([-1 1],[0.5 0.5],'Color',0.7*[1 1 1]);
        xlim([-1.1 1.1]);
        ylim([-0.05 1.05]);
        xlabel('Evidence');
        ylabel('Prob left choice');
        vr.myplots.perform_ax.TickDir = 'out';
        vr.myplots.perform_ax.Box = 'off';
        vr.myplots.perform_ax.PlotBoxAspectRatio = [1 1 1];
        hold off;
        
        % switch back to previous axis
        axes(tmp_ax);
end















