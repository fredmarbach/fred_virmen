% this function controls tower visibility
% tower is visible if 
%       mouseYPosition > towerYPosition - vr.S.GUI.triggerYOffset

function [vr] = towers_handling(vr,action)

switch action
    
    % -------------------------------------------------
    case 'init'
    % -------------------------------------------------    
        % find which objects are towers
        objectNames = fieldnames(vr.worlds{vr.currentWorld}.objects.indices);
        tmp_indices = ~cellfun(@isempty,regexp(objectNames,'t[0-9][0-9]'));
        vr.towers.names = objectNames(tmp_indices);
        vr.towers.nTowers = length(vr.towers.names);
        vr.towers.objectIndex = zeros(vr.towers.nTowers,1);
        vr.towers.y = zeros(vr.towers.nTowers,1);
        vr.towers.towerJustAppeared = false;
        vr.towers.visible = false(vr.towers.nTowers,1);
        for tt = 1:vr.towers.nTowers
            thisName = vr.towers.names{tt};
            thisObjectInd = vr.worlds{vr.currentWorld}.objects.indices.(thisName);
            vr.towers.objectIndex(tt) = thisObjectInd;
        end
    
        
    % -------------------------------------------------
    case 'updateVisibility'
    % -------------------------------------------------
        mouseYPosition = vr.position(2);
        vr.towers.towerJustAppeared = false; % turns true if conditions met
        for tt = 1:vr.towers.nActive
            towerYPos = vr.towers.y(tt);
            if mouseYPosition > towerYPos - vr.S.GUI.triggerYOffset && ... % mouse passed tower
                    vr.towers.visible(tt) == false % only make it visible if it wasn't already

                % indices of the first and last triangle of this tower
                tmp_ind = vr.towers.objectIndex(tt);
                triangleFirstLast = vr.worlds{vr.currentWorld}.objects.triangles(tmp_ind,:);
                allTriangles = triangleFirstLast(1):triangleFirstLast(2);
                      
                % signal that a tower appeared for photoDiode
                vr.towers.towerJustAppeared = true;

                % make all triangles of this tower visible
                vr.worlds{vr.currentWorld}.surface.visible(allTriangles) = true; 
                vr.towers.visible(tt) = true; 
            end
        end
     
        
    % -------------------------------------------------
    case 'makeAllInvisible'
    % -------------------------------------------------
        % used at beginning of each trial
        for tt = 1:vr.towers.nTowers
            % indices of the first and last triangle of this tower
            tmp_ind = vr.towers.objectIndex(tt);
            triangleFirstLast = vr.worlds{vr.currentWorld}.objects.triangles(tmp_ind,:);
            allTriangles = triangleFirstLast(1):triangleFirstLast(2);                   
            % make all triangles INvisible
            vr.worlds{vr.currentWorld}.surface.visible(allTriangles) = false;
            vr.towers.visible(tt) = false; 
        end
        
        
    % -------------------------------------------------
    case 'prepareTrial'
    % -------------------------------------------------    
        % tunnel dimensions
        totalLength = eval(vr.exper.variables.tunnelLength); % beginning of this is y coordinate = 0
        delayLength = vr.S.GUI.delayLength; % portion with no towers before decision
        evidenceLength = vr.S.GUI.evidenceLength; % portion occupied with towers
        preambleLength = vr.S.GUI.preambleLength; % portion before towers
        nextTrial = vr.tmp.currentTrial + 1;
        
        % tower y positions
        switch vr.S.GUIMeta.towerSpacing.String{vr.S.GUI.towerSpacing}
            case 'regular'
            
            case 'poisson'
                % poisson point process (following Pinto et al. 2018)
                % (from supplement; last two lines not implemented, looked wrong)
                
                newY = [];
                thisN = vr.params.towersLeftRight(nextTrial,:);
                for mm = 1:2 % loop over left and right side of tunnel
                    effectiveLength = evidenceLength - (thisN(mm)-1)*vr.S.GUI.minITD;
                    tmp_y = random('uniform',0,effectiveLength,[1,thisN(mm)]);
                    tmp_y = sort(tmp_y);
                    newY = [newY (tmp_y + vr.S.GUI.minITD*(0:thisN(mm)-1))];
                    nEachSide(mm) = thisN(mm);
                end
                
                if isempty(newY)
                    error('no towers');
                end
                vr.towers.mouseInitialY = totalLength - delayLength - ...
                                          max(newY) - preambleLength;
                vr.towers.y = newY + vr.towers.mouseInitialY + preambleLength;
                % -1 == left, 1 == right side
                vr.towers.sides = [-1*ones(1,nEachSide(1)) ones(1,nEachSide(2))];
                vr.towers.x = vr.towers.sides * eval(vr.exper.variables.floorWidth)/2;
                vr.towers.nActive = length(vr.towers.y);
        end % switch
        
        if vr.towers.mouseInitialY < 0
            warning('---tunnel not long enough: mouse placed at 0---');
            vr.towers.mouseInitialY = 0;
        end
        
        
    % -------------------------------------------------
    case 'placeTowers'
    % -------------------------------------------------   
        % move tower positions from vr.towers.y to vr.towers.nextY
        % note: vr.towers.nextY is in coordinates [0,tunnelLength]
        for tt = 1:vr.towers.nActive % loop over towers to be placed    
            goalY = vr.towers.y(tt);
            goalX = vr.towers.x(tt);

            % determine the indices of the vertices belonging to this tower
            thisObjectInd = vr.towers.objectIndex(tt);
            tmp_vertices = vr.worlds{vr.currentWorld}.objects.vertices(thisObjectInd,:);
            allVertices = tmp_vertices(1):tmp_vertices(2);

            % move tower to new y position
            tmp_y = vr.worlds{vr.currentWorld}.surface.vertices(2,allVertices);
            towerCentre = median(tmp_y);
            tmp_displacement = goalY - towerCentre;
            vr.worlds{vr.currentWorld}.surface.vertices(2,allVertices) = tmp_y + tmp_displacement;

            % move tower to new x position
            tmp_x = vr.worlds{vr.currentWorld}.surface.vertices(1,allVertices);
            towerCentre = median(tmp_x);
            tmp_displacement = goalX - towerCentre;
            vr.worlds{vr.currentWorld}.surface.vertices(1,allVertices) = tmp_x + tmp_displacement;
        end 
    
end











