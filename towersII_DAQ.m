% initialise DAQ

function vr = towersII_DAQ(vr,action)


switch action
    
    case 'init'
           
        % --- analog input (photoDiode, licks) ---
        vr.daq.ai_session = daq.createSession('ni');
        % ai0: photoDiode
        % ai1: TTL copy (sent to scanimage as trial start)
        % ai2/3: lick detector output left/right
        addAnalogInputChannel(vr.daq.ai_session,vr.S.GUI.deviceName,0:3,'Voltage');
        
        % tmp log file ID
        vr.daq.logFile = 'D:\temp_towersLog.bin';
        delete(vr.daq.logFile); % try to delete file
        if exist(vr.daq.logFile,'file') == 2
            fclose all;
            delete(vr.daq.logFile);
            disp('---closed all files to delete logFile---')
        end
        vr.daq.logFileID = fopen(vr.daq.logFile,'a'); % 'a' for append
        
        % add listener to save stuff in background
        vr.daq.ai_listener = addlistener(vr.daq.ai_session,'DataAvailable',...
                             @(src,event) fwrite(vr.daq.logFileID,...
                             [event.TimeStamps, event.Data]','double'));
        vr.daq.ai_session.IsContinuous = true;
        vr.daq.ai_session.Rate = 500;
        vr.daq.ai_session.NotifyWhenDataAvailableExceeds = 500;
        vr.daq.ai_session.startBackground();

        
        % --- counter input channels ---
        % 1 counter channel for the rotary encoder
        vr.daq.ctr_session = daq.createSession('ni');
        vr.daq.ctr0_channel = addCounterInputChannel(vr.daq.ctr_session,vr.S.GUI.deviceName,...
                                                'ctr0','Position');
        vr.daq.ctr0_channel.InitialCount = vr.mvt.initialCount;
        
        
        % --- digital out to scanimage at trial start ---
        % a copy is sent to analog input background recording
        vr.daq.out_session = daq.createSession('ni');
        warning('off','daq:Session:onDemandOnlyChannelsAdded');
        addDigitalChannel(vr.daq.out_session,vr.S.GUI.deviceName,...
                                                 'Port1/Line1:2','OutputOnly');

        % --- digital out to bpod ---
        % to tell it where the mouse is so it can react to licks etc
        % Port0/Line6: goes to WireIn1 on bpod (triggers start of tunnel)
        % Port0/Line7: goes to WireIn2 on bpod (triggers start of response period/end of tunnel)
        vr.daq.toBpod_session = daq.createSession('ni');
        addDigitalChannel(vr.daq.toBpod_session,vr.S.GUI.deviceName,...
                                                 'Port0/Line6:7','OutputOnly');     
                                             
        % --- digital input from bpod ---
        % tells us if mouse licked left or right
        % Port0/Line4: comes from Wire out2 on bpod (left response)
        % Port0/Line5: comes from Wire out3 on bpod (right response)
        vr.daq.fromBpod_session = daq.createSession('ni');
        addDigitalChannel(vr.daq.fromBpod_session,vr.S.GUI.deviceName,...
                                                 'Port0/Line4:5','InputOnly');
                                             
             
    case 'toBpod_start'
        outputSingleScan(vr.daq.toBpod_session,[1 0])
        pause(0.001) % pause for 1ms to make sure bpod catches it (maybe not necessary)
        outputSingleScan(vr.daq.toBpod_session,[0 0]) 
        
    case 'toBpod_end'
        outputSingleScan(vr.daq.toBpod_session,[0 1])
        pause(0.001) % pause for 1ms to make sure the ai session here catches it
        outputSingleScan(vr.daq.toBpod_session,[0 0]) 
        
    case 'fromBpod'
        vr.daq.fromBpod = vr.daq.fromBpod_session.inputSingleScan();
        
    case 'endOfTrial'
        
        % --- write to scanimage ---
        outputSingleScan(vr.daq.out_session,[1 1])
        pause(0.01) % pause for 10ms to make sure the ai session here catches it
        outputSingleScan(vr.daq.out_session,[0 0]) 
        
        % reset counters to initialcount 1000 to avoid too high values (which would probably never happen?)
        resetCounters(vr.daq.ctr_session);
        vr.mvt.currentCount = double(vr.daq.ctr0_channel.InitialCount);
        
        
    case 'cleanUp'
        
        % --- close sessions etc. ---
        vr.daq.ai_session.stop();
        delete(vr.daq.ai_listener);
        release(vr.daq.ai_session);
        fclose(vr.daq.logFileID);
        disp('---closed files and daq session---')
        
end
end












        