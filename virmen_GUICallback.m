% this function can be populated with callbacks
% for GUI parameters. It gets updated every trial

function vr = virmen_GUICallback(vr)

% --- compare current and previous trial S.GUI.___

thisP = vr.S.GUI; % this was just read afresh
prevP = vr.data.trial.GUI{vr.tmp.currentTrial}; % parameters of trial that just ended

fnames = fieldnames(prevP);
mismatches = {};
count = 1;
for ff = 1:length(fnames)
    if isstring(prevP.(fnames{ff}))
        if ~strcmp(prevP.(fnames{ff}),thisP.(fnames{ff}))
            mismatches{count} = fnames{ff};
            count = count + 1;
        end
    else
        if prevP.(fnames{ff}) ~= thisP.(fnames{ff})
            mismatches{count} = fnames{ff};
            count = count + 1;
        end
    end
end

% --- write callback functions here ---
for mm = 1:length(mismatches)
    
    switch mismatches{mm}
        case 'evidenceStrength'
            vr = towers_trials(vr,'updateAblauf');
            vr = towers_trialPlot(vr,'update');
        case 'maxSame'
            vr = towers_trials(vr,'updateAblauf');
            vr = towers_trialPlot(vr,'update');
        case 'leftProb'
            vr = towers_trials(vr,'updateAblauf');
            vr = towers_trialPlot(vr,'update');
            
    end
end























