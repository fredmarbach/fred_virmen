% code blatently stolen from Josh Sanders - Bpod (formerly called 'BpodParameterGUI')
% input:
%   S - all GUI parameters along with their meta info 
%   action - 'init' 'sync' 'get'
%   virmenGUI - contains all handles etc. created in case 'init'
%               pass this along if using 'sync' or 'get'
% output:
%   S - if using 'sync' this struct if filled with the current GUI values
%   virmenGUI - see above

function [virmenGUI,S] = virmen_updateGUI(S,action,varargin)

GUIHeight = 600;
GUIPosition = [40 10 1000 GUIHeight];

if nargin == 3
    virmenGUI = varargin{1};
end

switch action
    
    case 'init'
        ParamNames = fieldnames(S.GUI);
        nParams = length(ParamNames);
        virmenGUI.GUIData.ParameterGUI.ParamNames = cell(1,nParams);
        virmenGUI.GUIData.ParameterGUI.nParams = nParams;
        virmenGUI.GUIHandles.ParameterGUI.Labels = zeros(1,nParams);
        virmenGUI.GUIHandles.ParameterGUI.Params = cell(1,nParams);
        virmenGUI.GUIData.ParameterGUI.LastParamValues = cell(1,nParams);
        if isfield(S, 'GUIMeta')
            Meta = S.GUIMeta;
        else
            Meta = struct;
        end
        if isfield(S, 'GUIPanels')
            Panels = S.GUIPanels;
            PanelNames = fieldnames(Panels);
        else
            Panels = struct;
            Panels.Parameters = ParamNames;
            PanelNames = {'Parameters'};
        end
        if isfield(S,'GUITabs')
            Tabs = S.GUITabs;            
        else
            Tabs = struct;
            Tabs.Parameters = PanelNames;
        end
        TabNames = fieldnames(Tabs);
        nTabs = length(TabNames);
            
        PanelNames = PanelNames(end:-1:1);
        MaxVPos = 0;
        MaxHPos = 0;
        ParamNum = 1;
        virmenGUI.ProtocolFigures.ParameterGUI = figure('Position',GUIPosition,...
                                                         'name','Parameter GUI','numbertitle','off',...
                                                         'MenuBar','none','Resize','on');
        virmenGUI.GUIHandles.ParameterGUI.Tabs.TabGroup = uitabgroup(virmenGUI.ProtocolFigures.ParameterGUI);
        movegui(virmenGUI.ProtocolFigures.ParameterGUI,'southwest')
      
        for t = 1:nTabs
            VPos = 0;
            HPos = 0;
            ThisTabPanelNames = Tabs.(TabNames{t});
            nPanels = length(ThisTabPanelNames);
            virmenGUI.GUIHandles.ParameterGUI.Tabs.(TabNames{t}) = uitab('title',TabNames{t});
            htab = virmenGUI.GUIHandles.ParameterGUI.Tabs.(TabNames{t});
            for pp = 1:nPanels
                ThisPanelParamNames = Panels.(ThisTabPanelNames{pp});
                ThisPanelParamNames = ThisPanelParamNames(end:-1:1);
                nParams = length(ThisPanelParamNames);
                ThisPanelHeight = (45*nParams)+5;
                virmenGUI.GUIHandles.ParameterGUI.Panels.(ThisTabPanelNames{pp}) = uipanel(htab,'title',ThisTabPanelNames{pp},...
                                                                                           'FontSize',12,'FontWeight','Bold',...
                                                                                           'BackgroundColor','white','Units','Pixels',...
                                                                                           'Position',[HPos VPos 430 ThisPanelHeight]);
                InPanelPos = 10;
                for ii = 1:nParams
                    ThisParamName = ThisPanelParamNames{ii};
                    ThisParam = S.GUI.(ThisParamName);
                    virmenGUI.GUIData.ParameterGUI.ParamNames{ParamNum} = ThisParamName;
                    if ischar(ThisParam)
                        virmenGUI.GUIData.ParameterGUI.LastParamValues{ParamNum} = NaN;
                    else
                        virmenGUI.GUIData.ParameterGUI.LastParamValues{ParamNum} = ThisParam;
                    end
                    if isfield(Meta, ThisParamName)
                        if isstruct(Meta.(ThisParamName))
                            if isfield(Meta.(ThisParamName), 'Style')
                                ThisParamStyle = Meta.(ThisParamName).Style;
                                if isfield(Meta.(ThisParamName), 'String')
                                    ThisParamString = Meta.(ThisParamName).String;
                                else
                                    ThisParamString = '';
                                end
                            else
                                error(['Style not specified for parameter ' ThisParamName '.'])
                            end
                        else
                            error(['GUIMeta entry for ' ThisParamName ' must be a struct.'])
                        end
                    else
                        ThisParamStyle = 'edit';
                        ThisParamValue = NaN;
                    end
                    virmenGUI.GUIHandles.ParameterGUI.Labels(ParamNum) = uicontrol(htab,'Style','text','String',ThisParamName,...
                                                                                    'Position',[HPos+5 VPos+InPanelPos 200 25],'FontWeight','normal',...
                                                                                    'FontSize',12,'BackgroundColor','white','FontName','Arial',...
                                                                                    'HorizontalAlignment','Center');
                    switch lower(ThisParamStyle)
                        case 'edit'
                            virmenGUI.GUIData.ParameterGUI.Styles(ParamNum) = 1;
                            virmenGUI.GUIHandles.ParameterGUI.Params{ParamNum} = uicontrol(htab,'Style', 'edit', 'String', num2str(ThisParam), 'Position', [HPos+220 VPos+InPanelPos+2 200 25], 'FontWeight', 'normal', 'FontSize', 12, 'BackgroundColor','white', 'FontName', 'Arial','HorizontalAlignment','Center');
                        case 'edittext'
                            virmenGUI.GUIData.ParameterGUI.Styles(ParamNum) = 8;
                            virmenGUI.GUIHandles.ParameterGUI.Params{ParamNum} = uicontrol(htab,'Style', 'edit', 'String', ThisParam, 'Position', [HPos+220 VPos+InPanelPos+2 200 25], 'FontWeight', 'normal', 'FontSize', 12, 'BackgroundColor','white', 'FontName', 'Arial','HorizontalAlignment','Center');
                        case 'text'
                            virmenGUI.GUIData.ParameterGUI.Styles(ParamNum) = 2;
                            virmenGUI.GUIHandles.ParameterGUI.Params{ParamNum} = uicontrol(htab,'Style', 'text', 'String', num2str(ThisParam), 'Position', [HPos+220 VPos+InPanelPos+2 200 25], 'FontWeight', 'normal', 'FontSize', 12, 'BackgroundColor','white', 'FontName', 'Arial','HorizontalAlignment','Center');
                        case 'checkbox'
                            virmenGUI.GUIData.ParameterGUI.Styles(ParamNum) = 3;
                            virmenGUI.GUIHandles.ParameterGUI.Params{ParamNum} = uicontrol(htab,'Style', 'checkbox', 'Value', ThisParam, 'String', '   (check to activate)', 'Position', [HPos+220 VPos+InPanelPos+4 200 25], 'FontWeight', 'normal', 'FontSize', 12, 'BackgroundColor','white', 'FontName', 'Arial','HorizontalAlignment','Center');
                        case 'popupmenu'
                            virmenGUI.GUIData.ParameterGUI.Styles(ParamNum) = 4;
                            virmenGUI.GUIHandles.ParameterGUI.Params{ParamNum} = uicontrol(htab,'Style', 'popupmenu', 'String', ThisParamString, 'Value', ThisParam, 'Position', [HPos+220 VPos+InPanelPos+2 200 25], 'FontWeight', 'normal', 'FontSize', 12, 'BackgroundColor','white', 'FontName', 'Arial','HorizontalAlignment','Center');
                        case 'togglebutton' % INCOMPLETE
                            virmenGUI.GUIData.ParameterGUI.Styles(ParamNum) = 5;
                            virmenGUI.GUIHandles.ParameterGUI.Params{ParamNum} = uicontrol(htab,'Style', 'togglebutton', 'String', ThisParamString, 'Value', ThisParam, 'Position', [HPos+220 VPos+InPanelPos+2 200 25], 'FontWeight', 'normal', 'FontSize', 12, 'BackgroundColor','white', 'FontName', 'Arial','HorizontalAlignment','Center');
                        case 'pushbutton'
                            virmenGUI.GUIData.ParameterGUI.Styles(ParamNum) = 6;
                            virmenGUI.GUIHandles.ParameterGUI.Params{ParamNum} = uicontrol(htab,'Style', 'pushbutton', 'String', ThisParamString,...
                                'Value', ThisParam, 'Position', [HPos+220 VPos+InPanelPos+2 200 25], 'FontWeight', 'normal', 'FontSize', 12,...
                                'BackgroundColor','white', 'FontName', 'Arial','HorizontalAlignment','Center','Callback',Meta.OdorSettings.Callback);
                        case 'table'
                            virmenGUI.GUIData.ParameterGUI.Styles(ParamNum) = 7;
                            columnNames = fieldnames(S.GUI.(ThisParamName));
                            if isfield(Meta.(ThisParamName),'ColumnLabel')
                                columnLabel = Meta.(ThisParamName).ColumnLabel;
                            else
                                columnLabel = columnNames;
                            end
                            tableData = [];
                            for iTableCol = 1:numel(columnNames)
                                tableData = [tableData, S.GUI.(ThisParamName).(columnNames{iTableCol})];
                            end
%                             tableData(:,2) = tableData(:,2)/sum(tableData(:,2));
                            htable = uitable(htab,'data',tableData,'columnname',columnLabel,...
                                             'ColumnEditable',true(1,numel(columnLabel)), 'FontSize', 12);
                            htable.Position([3 4]) = htable.Extent([3 4]);
                            htable.Position([1 2]) = [HPos+220 VPos+InPanelPos+2];
                            virmenGUI.GUIHandles.ParameterGUI.Params{ParamNum} = htable;
                            ThisPanelHeight = ThisPanelHeight + (htable.Position(4)-25);
                            virmenGUI.GUIHandles.ParameterGUI.Panels.(ThisTabPanelNames{pp}).Position(4) = ThisPanelHeight;
                            virmenGUI.GUIData.ParameterGUI.LastParamValues{ParamNum} = htable.Data;
                        otherwise
                            error('Invalid parameter style specified. Valid parameters are: ''edit'', ''text'', ''checkbox'', ''popupmenu'', ''togglebutton'', ''pushbutton''');
                    end
                    InPanelPos = InPanelPos + 35;
                    ParamNum = ParamNum + 1;
                end
                % Check next panel to see if it will fit, otherwise start new column
                Wrap = 0;
                if pp < nPanels
                    NextPanelParams = Panels.(ThisTabPanelNames{pp+1});
                    NextPanelSize = (length(NextPanelParams)*45) + 5;
                    if VPos + ThisPanelHeight + 45 + NextPanelSize > GUIHeight
                        Wrap = 1;
                    end
                end
                VPos = VPos + ThisPanelHeight + 10;
                if Wrap
                    HPos = HPos + 450;
                    if VPos > MaxVPos
                        MaxVPos = VPos;
                    end
                    VPos = 10;
                else
                    if VPos > MaxVPos
                        MaxVPos = VPos;
                    end
                end
                if HPos > MaxHPos
                    MaxHPos = HPos;
                end
                set(virmenGUI.ProtocolFigures.ParameterGUI,'Position',[50 50 MaxHPos+500 MaxVPos+50]);
            end            
        end   
        
        
        
    case 'sync'
        ParamNames = virmenGUI.GUIData.ParameterGUI.ParamNames;
        nParams = virmenGUI.GUIData.ParameterGUI.nParams;
        for pp = 1:nParams
            ThisParamName = ParamNames{pp};
            ThisParamStyle = virmenGUI.GUIData.ParameterGUI.Styles(pp);
            ThisParamHandle = virmenGUI.GUIHandles.ParameterGUI.Params{pp};
            ThisParamLastValue = virmenGUI.GUIData.ParameterGUI.LastParamValues{pp};
            switch ThisParamStyle
                case 1 % Edit
                    GUIParam = str2double(get(ThisParamHandle, 'String'));
                    if GUIParam ~= ThisParamLastValue
                        S.GUI.(ThisParamName) = GUIParam;
                    elseif S.GUI.(ThisParamName) ~= ThisParamLastValue
                        set(ThisParamHandle, 'String', num2str(GUIParam));
                    end
                case 8 % Edit Text
                    GUIParam = get(ThisParamHandle, 'String');
                    if ~strcmpi(GUIParam, ThisParamLastValue)
                        S.GUI.(ThisParamName) = GUIParam;
                    elseif ~strcmpi(S.GUI.(ThisParamName), ThisParamLastValue)
                        set(ThisParamHandle, 'String', GUIParam);
                    end                                     
                case 2 % Text
                    GUIParam = S.GUI.(ThisParamName);
                    Text = GUIParam;
                    if ~ischar(Text)
                        Text = num2str(Text);
                    end
                    set(ThisParamHandle, 'String', Text);
                case 3 % Checkbox
                    GUIParam = get(ThisParamHandle, 'Value');
                    if GUIParam ~= ThisParamLastValue
                        S.GUI.(ThisParamName) = GUIParam;
                    elseif S.GUI.(ThisParamName) ~= ThisParamLastValue
                        set(ThisParamHandle, 'Value', GUIParam);
                    end
                case 4 % Popupmenu
                    GUIParam = get(ThisParamHandle, 'Value');
                    if GUIParam ~= ThisParamLastValue
                        S.GUI.(ThisParamName) = GUIParam;
                    elseif S.GUI.(ThisParamName) ~= ThisParamLastValue
                        set(ThisParamHandle, 'Value', GUIParam);
                    end
                case 6 %Pushbutton
                    GUIParam = get(ThisParamHandle, 'Value');
                    if GUIParam ~= ThisParamLastValue
                        S.GUI.(ThisParamName) = GUIParam;
                    elseif S.GUI.(ThisParamName) ~= ThisParamLastValue
                        set(ThisParamHandle, 'Value', GUIParam);
                    end
                case 7 %Table
                    GUIParam = ThisParamHandle.Data;
                    columnNames = fieldnames(S.GUI.(ThisParamName));
                    argData = [];
                    for iColumn = 1:numel(columnNames)
                        argData = [argData, S.GUI.(ThisParamName).(columnNames{iColumn})];
                    end
                    if any(GUIParam(:) ~= ThisParamLastValue(:)) % Change originated in the GUI propagates to TaskParameters
                        for iColumn = 1:numel(columnNames)
                            S.GUI.(ThisParamName).(columnNames{iColumn}) = GUIParam(:,iColumn);
                        end
                    elseif any(argData(:) ~= ThisParamLastValue(:)) % Change originated in TaskParameters propagates to the GUI
                        ThisParamHandle.Data = argData;
                    end
            end
            virmenGUI.GUIData.ParameterGUI.LastParamValues{pp} = GUIParam;
        end
        
        
    case 'get'
        ParamNames = virmenGUI.GUIData.ParameterGUI.ParamNames;
        nParams = virmenGUI.GUIData.ParameterGUI.nParams;
        for pp = 1:nParams
            ThisParamName = ParamNames{pp};
            ThisParamStyle = virmenGUI.GUIData.ParameterGUI.Styles(pp);
            ThisParamHandle = virmenGUI.GUIHandles.ParameterGUI.Params{pp};
            switch ThisParamStyle
                case 1 % Edit
                    GUIParam = str2double(get(ThisParamHandle, 'String'));
                    S.GUI.(ThisParamName) = GUIParam;
                case 8 % Edit Text
                    GUIParam = get(ThisParamHandle, 'String');
                    S.GUI.(ThisParamName) = GUIParam;                    
                case 2 % Text
                    GUIParam = get(ThisParamHandle, 'String');
                    GUIParam = str2double(GUIParam);  
                    S.GUI.(ThisParamName) = GUIParam;
                case 3 % Checkbox
                    GUIParam = get(ThisParamHandle, 'Value');
                    S.GUI.(ThisParamName) = GUIParam;
                case 4 % Popupmenu
                    GUIParam = get(ThisParamHandle, 'Value');
                    S.GUI.(ThisParamName) = GUIParam;
                case 6 % Pushbutton
                    GUIParam = get(ThisParamHandle, 'Value');
                    S.GUI.(ThisParamName) = GUIParam;
                case 7 % Table
                    GUIParam = ThisParamHandle.Data;
                    columnNames = fieldnames(S.GUI.(ThisParamName));
                    for iColumn = 1:numel(columnNames)
                         S.GUI.(ThisParamName).(columnNames{iColumn}) = GUIParam(:,iColumn);
                    end
            end
        end
    otherwise
    error('ParameterGUI must be called with a valid op code: ''init'' or ''sync''');
end


function SettingsMenuSave_Callback(~, ~, ~)
global virmenGUI
global TaskParameters
ProtocolSettings = BpodParameterGUI('get',TaskParameters);
save(virmenGUI.SettingsPath,'ProtocolSettings')

function SettingsMenuSaveAs_Callback(~, ~, SettingsMenuHandle)
global virmenGUI
global TaskParameters
ProtocolSettings = BpodParameterGUI('get',TaskParameters);
[file,path] = uiputfile('*.mat','Select a Bpod ProtocolSettings file.',virmenGUI.SettingsPath);
if file>0
    save(fullfile(path,file),'ProtocolSettings')
    virmenGUI.SettingsPath = fullfile(path,file);
    [~,SettingsName] = fileparts(file);
    set(SettingsMenuHandle,'Label',['Settings: ',SettingsName,'.']);
end

