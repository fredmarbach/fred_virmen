
function vr = towers_endOfTrial(vr,action)

switch action
    
    % -------------------------------------------------
    case 'checkIfEnd'
    % -------------------------------------------------
    
        % --- check if endOfTrial ---
        if vr.position(1) < -vr.S.GUI.xThreshold % mouse went left
            vr.tmp.endOfTrial = true;
            vr.data.trial.outcome(vr.tmp.currentTrial) = 1;
            if vr.params.actualEvidence(vr.tmp.currentTrial) > 0
                vr.data.trial.correct(vr.tmp.currentTrial) = 1;
            else
                vr.data.trial.correct(vr.tmp.currentTrial) = 0;
            end
        elseif vr.position(1) > vr.S.GUI.xThreshold % mouse went right
            vr.tmp.endOfTrial = true;
            vr.data.trial.outcome(vr.tmp.currentTrial) = 2;
            if vr.params.actualEvidence(vr.tmp.currentTrial) <= 0
                vr.data.trial.correct(vr.tmp.currentTrial) = 1;
            else
                vr.data.trial.correct(vr.tmp.currentTrial) = 0;
            end
        elseif vr.timeElapsed - vr.tmp.trialStartTime > vr.S.GUI.maxTrialTime % mouse timed out
            vr.tmp.endOfTrial = true;
            vr.data.trial.outcome(vr.tmp.currentTrial) = -1;
            vr.data.trial.correct(vr.tmp.currentTrial) = -1;
        else
            vr.tmp.endOfTrial = false;
        end
        
        
    % -------------------------------------------------
    case 'endOfTrial'
    % -------------------------------------------------
        % --- display code execution time ---
        fprintf('--- end of trial %d: (avg/min/max) \n',vr.tmp.currentTrial)
        tmp_iter = diff(vr.data.time(vr.data.trialNumber==vr.tmp.currentTrial));
        fprintf('      iteration time %1.3f/%1.3f/%1.3f \n',...
                mean(tmp_iter),min(tmp_iter),max(tmp_iter))
        fprintf('      runtime code %1.4f/%1.4f/%1.4f \n',...
                mean(vr.tmp.executionTime),min(vr.tmp.executionTime),max(vr.tmp.executionTime));
        vr.tmp.executionTime = 0;
        vr.tmp.executionInd = 0;

        % --- increment trial number ---
        vr.tmp.currentTrial = vr.tmp.currentTrial + 1;
        vr.tmp.trialStartTime = vr.timeElapsed;
        
        % --- turn on ITI / black flag ---
        % towers_ITItunnel uses this as cue to switch to ITI tunnel and handle punish condition
        vr.tmp.ITI = true;
        % make world black, changes vr.tmp.black == true
        vr = virmen_black(vr,'black');
        
        % --- put mouse at 0 to avoid crossing threshold right away ---
        vr.position([1 2 4]) = 0;
 
end




















