% plot a circle

function h = drawCircle(x,y,r,c)

th = 0:pi/5:2*pi;
xunit = r * cos(th) + x;
yunit = r * sin(th) + y;
h = plot(xunit,yunit,'Color',c);
