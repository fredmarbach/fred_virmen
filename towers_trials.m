
function vr = towers_trials(vr,action)


switch action
    
    % -------------------------------------------------
    case 'updateAblauf'
    % -------------------------------------------------
        
        if vr.tmp.currentTrial == 1
            remainingTrials = 1:vr.S.GUI.maxTrials;
            vr.data.trial.outcome = zeros(1,vr.S.GUI.maxTrials); % for plotting
            vr.data.trial.correct = nan(1,vr.S.GUI.maxTrials); % for plotting
        else
            % called at the end of trial (vr.tmp.currentTrial is updated at
            % the end of the endOfTrial routines); thus the next trial is
            % vr.tmp.currentTrial + 1
            remainingTrials = vr.tmp.currentTrial+1:vr.S.GUI.maxTrials;
        end
        

        % evidenceStrength in percent vr.towers.towerRate (joint (both sides) maximum rate of towers appearing)
        tmp_strengths = strsplit(vr.S.GUIMeta.evidenceStrength.String{vr.S.GUI.evidenceStrength},' ');
        nStrengths = length(tmp_strengths);
        randomIndex = randi([1 nStrengths],length(remainingTrials),1);
        vr.params.evidenceStrength(remainingTrials) = cellfun(@str2double,tmp_strengths(randomIndex));
        
        % left-right distribution (left == 1)
        for ii = remainingTrials
            if ii > vr.S.GUI.maxSame
                % look at previous 'maxSame' number of trials
                prevTrials = ii-vr.S.GUI.maxSame:ii-1;
                if sum(vr.params.leftTrials(prevTrials)) == vr.S.GUI.maxSame || ...
                        sum(vr.params.leftTrials(prevTrials)) == 0
                    vr.params.leftTrials(ii) = ~vr.params.leftTrials(ii-1);
                else
                    vr.params.leftTrials(ii) = rand(1) < vr.S.GUI.leftProb;
                end
            else
                vr.params.leftTrials(ii) = rand(1) < vr.S.GUI.leftProb;
            end
        end
                        
        % determine tower rate on left side of tunnel for each trial
        for ii = remainingTrials
            if vr.params.leftTrials(ii) == 1
                thisStrength = vr.params.evidenceStrength(ii)/100;
            else
                thisStrength = (100 - vr.params.evidenceStrength(ii))/100;
            end
            vr.params.leftSideRate(ii) = thisStrength * vr.S.GUI.towerRate;
        end
        
        % --- draw tower number N for each side on each trial ---
        switch vr.S.GUIMeta.towerSpacing.String{vr.S.GUI.towerSpacing}
            case 'regular'
            
            case 'poisson'
                % poisson point process (following Pinto et al. 2018)
                % (from supplement; last two lines not implemented, looked wrong)
                maxN = floor(vr.S.GUI.evidenceLength / vr.S.GUI.minITD);
                for ii = remainingTrials
                    mu(1) = (vr.S.GUI.evidenceLength/1000) * ...
                             vr.params.leftSideRate(ii); % in towers/meter
                    mu(2) = (vr.S.GUI.evidenceLength/1000) * vr.S.GUI.towerRate - mu(1);
                    
                    for mm = 1:2 % loop over left and right side of tunnel
                        N(mm) = 1000; % number of towers
                        if mu(mm) > 0
                            while N(mm) > maxN || N(mm) < 1 % make sure there is at least 1 tower
                                N(mm) = random('poisson',mu(mm));
                            end
                        else
                            N(mm) = 0;
                        end
                    end
                    % make sure there is at least 1 tower
                    if sum(N) == 0 && mu(1) > mu(2)
                        N(1) = 1;
                    elseif sum(N) == 0 && mu(1) < mu(2)
                        N(2) = 1;
                    end
                        
                    vr.params.actualEvidence(ii) = (N(1)-N(2))/sum(N);
                    vr.params.towersLeftRight(ii,:) = N;
                end
        end % switch
   
        
    % -------------------------------------------------
    case 'antiBias'
    % -------------------------------------------------
        if vr.tmp.currentTrial > 2 % only do this after 3 trials
            
            % --- execute chosen antiBias routine ---
            switch vr.S.GUIMeta.antiBias.String{vr.S.GUI.antiBias}
                
                case 'gauss'
                    % weights: half gaussian window over past nBack trials
                    % this std is approximate, check gausswin for explanation
                    switch vr.S.GUIMeta.windowStd.String{vr.S.GUI.windowStd}
                        case 'small'
                            thisWidth = 4;
                        case 'med'
                            thisWidth = 2;
                        case 'large' 
                            thisWidth = 1;
                    end
                    w = gausswin(vr.S.GUI.nBack*2,thisWidth)';
                    nBackTrials = max(1,vr.tmp.currentTrial-vr.S.GUI.nBack+1):vr.tmp.currentTrial;
                    % second half of gaussian, flipped (w(end) close to 1, which will be multiplied with the currentTrial
                    w = fliplr(w(vr.S.GUI.nBack+1:vr.S.GUI.nBack+1+length(nBackTrials)-1)); 
                    % error rate for left and right trials
                    tmp_left = vr.params.leftTrials(nBackTrials);
                    tmp_correct = vr.data.trial.correct(nBackTrials);
                    errorR = 0; 
                    errorL = 0;
                    tmp = tmp_left == 0 & tmp_correct == 0;
                    if ~isempty(tmp) && sum(tmp_left==0) > 0
                        errorR = sum(tmp.*w) / sum(tmp_left==0);
                    end
                    tmp = tmp_left == 1 & tmp_correct == 0;
                    if ~isempty(tmp) && sum(tmp_left==1) > 0
                        errorL = sum(tmp.*w) / sum(tmp_left==1);
                    end

                    % assign leftProb and cap it between given values  
                    if errorR+errorL == 0
                        vr.S.GUI.leftProb = 0.5;
                    else
                        leftProb = sqrt(errorL) / (sqrt(errorR) + sqrt(errorL));
                        if leftProb > vr.S.GUI.probCap
                            vr.S.GUI.leftProb = vr.S.GUI.probCap;
                        elseif leftProb < 1-vr.S.GUI.probCap
                            vr.S.GUI.leftProb = 1-vr.S.GUI.probCap;
                        else
                            vr.S.GUI.leftProb = leftProb;
                        end
                    end
                    
                    % update trials (trialPlot is updated every trial anyways)
                    vr = towers_trials(vr,'updateAblauf');

                case 'repeat'
                    if vr.data.trial.correct(vr.tmp.currentTrial) == 0 % error trial
                        if vr.params.actualEvidence(vr.tmp.currentTrial) > 0 % left trial
                            vr.S.GUI.leftProb = 1; % force more left trials
                        elseif vr.params.actualEvidence(vr.tmp.currentTrial) < 0 % right trial
                            vr.S.GUI.leftProb = 0; % force more right trials
                        end
                    else % correct trial
                        vr.S.GUI.leftProb = 0.5;
                    end
                    
                    % update trials (trialPlot is updated every trial anyways)
                    vr = towers_trials(vr,'updateAblauf');

                case 'none'
                    vr.S.GUI.leftProb = 0.5;
            end

        end
end






















