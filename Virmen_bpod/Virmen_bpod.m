function Virmen_bpod


global BpodSystem 

% assign port numbers
leftPort = 1;
rightPort = 2;

% reward amount
rewardAmount = 4;

% get valve durations based on calibration
tmp = GetValveTimes(rewardAmount, [leftPort rightPort]); 
leftValveTime = tmp(1);
rightValveTime = tmp(2);

% parameters for lick plot
plotParams.dispNTrials = 50;
plotParams.colorL = [0.2 0.1 0.7];
plotParams.colorR = [0.6 0.1 0.5];
plotParams.markerSize = 5;
plotParams.f1 = figure;
plotParams.ax = axes(plotParams.f1);
plot([0 0],[0 1000],'--','Color',0.5*[1 1 1],...
                    'Parent',plotParams.ax);
plotParams.ax.NextPlot = 'add';
plotParams.ax.XLabel.String = 'Time from end of Tunnel [s]';
plotParams.ax.YLabel.String = 'Trial number';
plotParams.ax.TickDir = 'out';

% initialise bpod plugin
TotalRewardDisplay('init');

% ------------------------------
% --- main trial loop ---
% ------------------------------
for currentTrial = 1:1000
    
% --- assemble state matrix ---
 	sma = NewStateMatrix();
    
    % --- wait for mouse to start tunnel ---
    sma = AddState(sma,'Name','waitForTrigger1',...
        'Timer',0,...
        'StateChangeConditions',{'Wire1High','waitForTrigger2',...
                                 'Wire2High','waitForTrigger2_freeWater',...
                                 'BNC1High','exit'},...
        'OutputActions',{});
    
    % ---------------------------
    % --- normal state matrix ---
    % ---------------------------
    % --- wait for mouse to reach end of tunnel ---
    sma = AddState(sma,'Name','waitForTrigger2',...
        'Timer',0,...
        'StateChangeConditions',{'Wire1High','waitForLick_L',...
                                 'Wire2High','waitForLick_R',...
                                 'BNC1High','exit'},...
        'OutputActions',{});
    
    % --- reacting to licks ---
    % if no licks occur, virmen will send a TTL to abort the trial
    sma = AddState(sma,'Name','waitForLick_L',...
        'Timer',0,...
        'StateChangeConditions',{['Port' num2str(leftPort) 'In'],'leftReward',...
                                 ['Port' num2str(rightPort) 'In'],'end_abort',...
                                 'BNC1High','exit'},...
        'OutputActions',{});
    sma = AddState(sma,'Name','waitForLick_R',...
        'Timer',0,...
        'StateChangeConditions',{['Port' num2str(leftPort) 'In'],'end_abort',...
                                 ['Port' num2str(rightPort) 'In'],'rightReward',...
                                 'BNC1High','exit'},...
        'OutputActions',{});
    
    % --- Reward delivery ---
    sma = AddState(sma,'Name','leftReward',...
        'Timer',leftValveTime,...
        'StateChangeConditions',{'Tup','last_state',...
                                 'BNC1High','exit'},...
        'OutputActions',{});
    sma = AddState(sma,'Name','rightReward',...
        'Timer',rightValveTime,...
        'StateChangeConditions',{'Tup','last_state',...
                                 'BNC1High','exit'},...
        'OutputActions',{});
    
    % --- end states ---
    % virmen does a InputSingleScan every iteration
    % if WireState is high it sends BNC1High to start the next trial
    sma = AddState(sma,'Name','end_left',...
        'Timer',0,...
        'StateChangeConditions',{'BNC1High','exit'},...
        'OutputActions',{'WireState',2^(2-1)});
    sma = AddState(sma,'Name','end_right',...
        'Timer',0,...
        'StateChangeConditions',{'BNC1High','exit'},...
        'OutputActions',{'WireState',2^(3-1)});
    sma = AddState(sma,'Name','end_abort',...
        'Timer',0,...
        'StateChangeConditions',{'BNC1High','exit'},...
        'OutputActions',{});
    
    % -----------------------------
    % --- direct water delivery ---
    % -----------------------------
    % --- wait for mouse to reach end of tunnel ---
    sma = AddState(sma,'Name','waitForTrigger2_freeWater',...
        'Timer',0,...
        'StateChangeConditions',{'Wire1High','waitForLick_L_freeWater',...
                                 'Wire2High','waitForLick_R_freeWater',...
                                 'BNC1High','exit'},...
        'OutputActions',{});
    
    % --- reacting to licks ---
    % if no licks occur, virmen will send a TTL to abort the trial
    sma = AddState(sma,'Name','waitForLick_L_freeWater',...
        'Timer',leftValveTime,...
        'StateChangeConditions',{['Port' num2str(leftPort) 'In'],'leftReward',...
                                 ['Port' num2str(rightPort) 'In'],'end_abort',...
                                 'BNC1High','exit'},...
        'OutputActions',{});
    sma = AddState(sma,'Name','waitForLick_R_freeWater',...
        'Timer',0,...
        'StateChangeConditions',{['Port' num2str(leftPort) 'In'],'end_abort',...
                                 ['Port' num2str(rightPort) 'In'],'rightReward',...
                                 'BNC1High','exit'},...
        'OutputActions',{});
    
    % --- Reward delivery ---
    sma = AddState(sma,'Name','leftReward',...
        'Timer',leftValveTime,...
        'StateChangeConditions',{'Tup','last_state',...
                                 'BNC1High','exit'},...
        'OutputActions',{});
    sma = AddState(sma,'Name','rightReward',...
        'Timer',rightValveTime,...
        'StateChangeConditions',{'Tup','last_state',...
                                 'BNC1High','exit'},...
        'OutputActions',{});
    
    
% --- send and run state matrix ---
    SendStateMatrix(sma);
    RawEvents = RunStateMatrix;
    
% --- interpret raw events ---
    data_tmp = struct;
    data_tmp = AddTrialEvents(data_tmp,RawEvents); % Computes trial events from raw data
    actualRawEvents = data_tmp.RawEvents.Trial{1};
    
% --- plot licks aligned to trigger from virmen ---
    updateLickPlot(actualRawEvents,currentTrial,...
                   plotParams,leftPort,rightPort);
    
% --- update total reward ---
    % only if correct trial
    if ~isnan(actualRawEvents.States.leftReward(1)) || ...
       ~isnan(actualRawEvents.States.rightReward(1))
        TotalRewardDisplay('add',rewardAmount);
    end
    
    HandlePauseCondition; % Checks to see if the protocol is paused. If so, waits until user resumes.

    if BpodSystem.Status.BeingUsed == 0
        return
    end
end
end


% ------------------------------
% --- function to plot licks ---
% ------------------------------
function [] = updateLickPlot(RawEvents,currentTrial,plotParams,leftPort,rightPort)
    
    if isfield(RawEvents,'States')
        s = RawEvents.States;
    else
        s = nan;
    end
    if isfield(RawEvents,'Events')
        e = RawEvents.Events;
    else
        e = nan;
    end
    
    leftInput = ['Port' num2str(leftPort) 'In'];
    rightInput = ['Port' num2str(rightPort) 'In'];
    
    if isstruct(s) && isstruct(e)
        time_endTunnel = s.waitForTrigger2(2); % time the end of tunnel trigger came in
        time_startTunnel = s.waitForTrigger1(2)-time_endTunnel; % time the start of tunnel trigger came in

        if isfield(e,leftInput)
            licksLeft = e.(leftInput)-time_endTunnel;
        else
            licksLeft = nan;
        end
        if isfield(e,rightInput)
            licksRight = e.(rightInput)-time_endTunnel;
        else
            licksRight = nan;
        end

        % plot the licks
        yvec = currentTrial*ones(1,length(licksLeft));
        plot(licksLeft,yvec,'o','MarkerFaceColor',plotParams.colorL,...
                            'MarkerEdgeColor','none',...
                            'MarkerSize',plotParams.markerSize,...
                            'Parent',plotParams.ax);
        yvec = currentTrial*ones(1,length(licksLeft));
        plot(licksRight,yvec,'o','MarkerFaceColor',plotParams.colorR,...
                            'MarkerEdgeColor','none',...
                            'MarkerSize',plotParams.markerSize,...
                            'Parent',plotParams.ax);
        % plot marker at time of tunnel start
        plot(time_startTunnel,currentTrial,'d','MarkerFaceColor',0.5*[1 1 1],...
                            'MarkerEdgeColor','none',...
                            'MarkerSize',plotParams.markerSize,...
                            'Parent',plotParams.ax);

    end
    % update ylimits
    plotParams.ax.YLim = [max(0,currentTrial-plotParams.dispNTrials) ...
                          max(plotParams.dispNTrials,currentTrial)];
end






















