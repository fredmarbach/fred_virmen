% this turns everything off to invisible

function vr = virmen_black(vr,action)

switch action
    
    case 'black'
        if vr.tmp.black == false
            vr.tmp.currentVisibility = vr.worlds{vr.currentWorld}.surface.visible;
            vr.worlds{vr.currentWorld}.surface.visible(:) = false;
            vr.tmp.blackTime = vr.timeElapsed;
            vr.tmp.black = true;
        end

    case 'checkTime'
        if vr.tmp.black == true % make sure case 'black' was executed
            if vr.timeElapsed-vr.tmp.blackTime > vr.S.GUI.blackTime
                vr.worlds{vr.currentWorld}.surface.visible = vr.tmp.currentVisibility;
                vr.tmp.black = false;
            end        
        end
  
    case 'visible'
        if vr.tmp.black == true % make sure case 'black' was executed
            vr.worlds{vr.currentWorld}.surface.visible = vr.tmp.currentVisibility;
            vr.tmp.black = false;
        end
end