
function vr = towers_ITItunnel(vr,action)

switch action
    
    % -------------------------------------------------
    case 'ITI'
    % -------------------------------------------------
        % virmen_endOfTrial changes vr.tmp.ITI and vr.tmp.black to true

        if vr.tmp.black == true
            % change vr.tmp.black == false after vr.S.GUI.blackTime
            vr = virmen_black(vr,'checkTime');
        end

        % --- ITI is on and world visible ---
        if vr.tmp.black == false 
            if vr.currentWorld == 1 % not yet switched to ITI world
                % switch to ITI world
                vr.currentWorld = 2;
                % put mouse at starting position
                vr.position([1 4]) = 0;
                vr.dp(:) = 0; 
                if vr.data.trial.correct(vr.tmp.currentTrial-1) == 0 % error (-1 because currentTrial already got updated for next trial)
                    % put mouse at y position -vr.S.GUI.punish
                    vr.position(2) = -vr.S.GUI.punishLength;
                else % put mouse at +30 (to avoid seeing the punishTunnel)
                    vr.position(2) = 30;
                end  
            else % ITI running
                % keep heading direction straight for mouse to recover from turning
                vr.position(4) = 0;
            end

            % --- check if ITI is over ---
            if vr.position(2) > vr.S.GUI.ITILength
                vr.currentWorld = 1;
                vr.tmp.ITI = false;
                vr.position(1) = 0;
                vr.position(2) = vr.towers.mouseInitialY;
                vr.position(4) = 0.0001; % set view angle to very small value, this avoids a weird bug
            end
        end
    
end