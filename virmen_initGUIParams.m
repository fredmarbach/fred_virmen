
function vr = virmen_initGUIParams(vr)


% --- load settings from previous session ---
% if there is none, prompt to select settings from other mouse
% if prompt is canceled, S will be filled with default values below
[S,failedToLoad] = virmen_loadSettings(vr);
           

% S.GUIMeta.X.Style='popupmenu';
% S.GUIMeta.X.String=phaseName;
%  
% S.GUIMeta.X.Style='checkbox';
% S.GUIMeta.X.String='Auto';
                                       
% ------------------------------------------------
% --- initialise task-dependent parameters -------
% ------------------------------------------------
switch vr.params.task

    % ------------------------------
    case 'towers' 
    % ------------------------------
        % --- PANEL --- TaskSpecifics ---
        default.GUIPanels.TaskSpecifics = {'xThreshold'};
        tmp = eval(vr.exper.variables.floorWidth)/2;
        default.GUI.xThreshold = 1.2*tmp; % x value to cross entering the side arms
        default.GUI.nTrajVisible = 10;
        
    % ------------------------------     
    case 'towersII'
    % ------------------------------
        % --- PANEL --- TaskSpecifics ---
        default.GUIPanels.TaskSpecifics = {'yThreshold','nTrajVisible'};
        tmp = eval(vr.exper.variables.tunnelLength) + eval(vr.exper.variables.endWall_w)/2;
        default.GUI.yThreshold = 1.2*tmp; % x value to cross entering the side arms
        default.GUI.nTrajVisible = 10;
        
        % --------------------------------------    
end
 
% -----------------------------------------
% --- initialise general parameters -------
% -----------------------------------------
        
% ---define default parameters---
% these here are task independent/general
    
% --- overwrite experiment notes in any case ---
S.GUI.laserPower = '';
S.GUI.mouseWeight = '';
S.GUI.sessionNotes = '';

% --- PANEL --- Plots ---
default.GUIPanels.Plots = {'nTrialsVisible',...
                           'psychBin'};
default.GUI.nTrialsVisible = '-30 15';
default.GUI.psychBin = 0.333; 
default.GUIMeta.nTrialsVisible.Style = 'edittext';
default.GUIMeta.nTrialsVisible.String = 'Auto';

% --- PANEL --- antiBias ---
default.GUIPanels.antiBias = {'probCap','nBack',...
                              'windowStd','antiBias'};
default.GUI.probCap = 0.9;
default.GUI.nBack = 40;
default.GUI.windowStd = 2; 
default.GUIMeta.windowStd.Style = 'popupmenu';
default.GUIMeta.windowStd.String = {'small','med','large'};
default.GUIMeta.antiBias.Style = 'popupmenu';
default.GUIMeta.antiBias.String = {'none','gauss','repeat'};
default.GUI.antiBias = 1;
        
% --- PANEL --- TaskParams ---
default.GUIPanels.TaskParams = {'triggerYOffset','preambleLength',...
                                'punishLength','delayLength',...
                                'evidenceLength','towerSpacing',...
                                'towerRate','minITD',...
                                'evidenceStrength'};
default.GUI.triggerYOffset = 40;
default.GUI.preambleLength = 60;
default.GUI.punishLength = 20;
default.GUI.delayLength = 10;
default.GUI.evidenceLength = 500;
default.GUI.towerSpacing = 1;
default.GUIMeta.towerSpacing.Style = 'popupmenu';
default.GUIMeta.towerSpacing.String = {'poisson','regular'};
default.GUI.minITD = 20; % minimal distance between towers
default.GUI.towerRate = 10; % in towers/meter
default.GUI.evidenceStrength = 1;
default.GUIMeta.evidenceStrength.Style = 'popupmenu';
default.GUIMeta.evidenceStrength.String = {'100','100 100 80','100 80 65'};
    
% --- PANEL --- NiDaq ---
default.GUIPanels.NiDaq = {'deviceName','monitorNumber'};
default.GUIMeta.deviceName.Style = 'edittext';
default.GUIMeta.deviceName.String = 'Auto';
default.GUI.deviceName = 'Dev1';
default.GUI.monitorNumber = 3;   

% --- PANEL --- Transform ---
default.GUIPanels.Transform = {'monitorAngle','minDistX','minDistY',...
                         'monitorWidth','monitorHeight','mouseHeight',...
                         'xFactor','rFactor','lengthFactor'};
default.GUI.monitorAngle = 80; 
default.GUI.xFactor = 1;  
default.GUI.rFactor = 1;  
default.GUI.lengthFactor = 0.8;  
default.GUI.monitorWidth = 410;  
default.GUI.monitorHeight = 235;  
default.GUI.mouseHeight = 0.85;  
default.GUI.minDistX = 220;  
default.GUI.minDistY = 320;  

% --- PANEL --- Movement ---
default.GUIPanels.Movement = {'scalingX','scalingY','direction'};
default.GUI.scalingX = 1;
default.GUI.scalingY = 1;
default.GUI.direction = 1;

% --- PANEL --- General Parameters ---
default.GUIPanels.General = {'maxTrials','maxTrialTime',...
                             'maxSame','leftProb','blackTime',...
                             'punishLength','ITILength'};    
default.GUI.maxTrials = 300;
default.GUI.maxTrialTime = 60;
default.GUI.maxSame = 4;
default.GUI.leftProb = 0.5;
default.GUI.blackTime = 0.5; % how long the world turns black at end of trial
default.GUI.ITILength = 500; % in mm VR space
default.GUI.punishLength = 200; % in mm VR space, added to ITI

% --- PANEL --- Experiment Notes ---
% Experiment notes are not loaded from previous session
% so here they are initialised / overwrite loaded ones
default.GUIPanels.ExperimentNotes = {'laserPower','mouseWeight','sessionNotes'};
default.GUIMeta.laserPower.Style = 'edittext';
default.GUIMeta.laserPower.String = 'Auto';
default.GUIMeta.sessionNotes.Style = 'edittext';
default.GUIMeta.sessionNotes.String = 'Auto'; 
default.GUI.laserPower = '';
default.GUI.mouseWeight = '';
default.GUI.sessionNotes = '';


% ----------------------------------------------------
% --- TABS --- assemble GUI in order of appearance ---
default.GUITabs.Task = {'antiBias','General','TaskParams','TaskSpecifics'};
default.GUITabs.Data = {'NiDaq','Plots'};
default.GUITabs.World = {'Movement','Transform'};
default.GUITabs.Notes = {'ExperimentNotes'};


% --- complete/trim S by comparing it to default ---
% this assures that the saved mouse parameters remain compatible 
% if we change the GUI parameter arrangement and variables (default struct here)
% i) missing fields in S are copied over from default
S = matchFields(S,default,'copy');
% ii) existing fields in S are deleted if they are not present in default
S = matchFields(S,default,'remove');
% iii) GUI organisation related fields are copied from default
S.GUITabs = default.GUITabs;
S.GUIMeta = default.GUIMeta;
S.GUIPanels = default.GUIPanels;
% iv) make sure S.GUI and S.GUIPanels have the same parameters
virmen_checkGUIPanels(S);

% finally store S in vr
vr.S = S;




















